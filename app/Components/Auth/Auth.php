<?php

namespace App\Components\Auth;

use App;
use App\Database;
use App\Components\Auth\AbstractAuth;
use App\Model\UserModel;

class Auth extends AbstractAuth
{
    /** @var Database */
    private $database;

    /**
     * Auth constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        parent::__construct($database);

        $this->database = $database;
    }

    /**
     * Mathode de connexion de l'utilisateur a partir de son login et du mot de passe encoder en md5
     *
     * @param $login
     * @param $password
     *
     * @return boolean
     */
    public function login($login, $password)
    {
        // Hachage du mot de passe
        //Lors de l'inscription,
        // sha1  ,  md5  ,  sha256  ,  sha512  ... ne sont plus considérées comme des fonctions de hachage sûres aujourd'hui.
        // Il faut pas les utilisez. mais il faut utiliser password_hash qui choisit le meilleur algorithme pour nous.
        //$pass_hache = password_hash($_POST['pass'], PASSWORD_DEFAULT);

        //  Récupération de l'utilisateur et de son pass hashé
        $user = $this->database->prepare('SELECT * FROM users WHERE login = ?',
            [$login], null, true);


        // Comparaison du pass envoyé via le formulaire avec la base
        $isPasswordCorrect = password_verify($password, $user->password);

        if ($isPasswordCorrect) {
            $_SESSION['auth'] = [
                "id"    => $user->id,
                "login" => $user->login,
                "email" => $user->email
            ];

            return true;
        }

        return false;
    }
}