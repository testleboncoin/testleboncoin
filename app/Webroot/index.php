<?php
define('WEBROOT', str_replace("app/Webroot/index.php", "", $_SERVER["SCRIPT_NAME"]));
define('ROOT', str_replace("app/Webroot/index.php", "", $_SERVER["SCRIPT_FILENAME"]));
require ROOT . '/app/App.php';
require ROOT . 'router.php';
require ROOT . 'request.php';
require ROOT . 'dispatcher.php';
App::load();
$dispatch = new Dispatcher();
$dispatch->dispatch();


