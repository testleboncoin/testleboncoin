<?php

namespace App\Controllers;

use App\Controllers\ControllerInterface;
use App\Models\AddresseModel;
use App\Models\ContactModel;
use Exception;

class AddressController extends MainController implements ControllerInterface
{
    /**
     * @var ContactModel
     */
    protected $modelContact;

    /**
     * @var AddresseModel
     */
    protected $modelAddress;

    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->modelAddress = $this->loadModel('Addresse');
        $this->modelContact = $this->loadModel('Contact');
    }

    /**
     * Affichage de la liste des adresses d'un Utilisateur
     * @param int|null $iIdContact
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws Exception
     */
    public function index(int $iIdContact = null)
    {
        $contact = $this->modelContact->findById($iIdContact);
        if (empty($contact)) {
            throw new Exception('Id de contact n\'exisite pas');
        }

        $addresses = $this->modelAddress->getByContact($iIdContact);

        echo $this->twig->render('address/addresslist.html.twig', [
            'addresses' => $addresses,
            'contact'   => $contact
        ]);
    }

    /**
     * Ajout d'adresse pour un contact
     *
     *
     * @param int|null $iIdContact
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function add(int $iIdContact = null)
    {
        $contact = $this->modelContact->findById($iIdContact);
        if (empty($contact)) {
            throw new Exception('Id de contact n\'exisite pas');
        }

        $error = false;
        if (!empty($_POST)) {
            $response = $this->sanitize($_POST);
            if ($response["response"]) {
                $result = $this->modelAddress->create([
                    'number'     => $response['number'],
                    'city'       => $response['city'],
                    'country'    => $response['country'],
                    'postalCode' => $response['postalCode'],
                    'street'     => $response['street'],
                    'idContact'  => $iIdContact
                ]);
                if ($result) {
                    $this->redirect("Address/index", $iIdContact);
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }

        echo $this->twig->render('address/addressadd.html.twig', [
                                                            "idContact" => $iIdContact,
                                                            'error' => $error
                                                         ]);
    }

    /**
     * Modification d'une adresse d'un contact
     *
     * @param int|null $iIdAddress
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function edit(int $iIdAddress)
    {
        $oAddress = $this->modelAddress->findById($iIdAddress);
        if (empty($oAddress)) {
            throw new Exception('Id d\'adresse n\'exisite pas');
        }

        $error = false;
        if (!empty($_POST)) {
            $response = $this->sanitize($_POST);
            if ($response["response"]) {
                $result   = $this->modelAddress->update($iIdAddress,
                            [
                                'number'     => $response['number'],
                                'city'       => $response['city'],
                                'country'    => $response['country'],
                                'postalCode' => $response['postalCode'],
                                'street'     => $response['street'],
                            ]);
                if ($result) {
                    $this->redirect("Address/index", $response['idContact']);
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }

        echo $this->twig->render('address/addressedit.html.twig',
            [
                'error'      => $error,
                'address'    => $oAddress,
            ]);
    }

    /**
     * Suppression d'une adresse d'un contact
     * @param int $iIdAddress
     * @throws \Exception
     */
    public function delete(int $iIdAddress)
    {
        $oAddress = $this->modelAddress->findById($iIdAddress);
        if (empty($oAddress)) {
            throw new Exception('Id d\'adresse n\'exisite pas');
        }

        $result = $this->modelAddress->delete($iIdAddress);
        if ($result) {
            $this->redirect("Address/index", $oAddress->idContact);
        }
    }

    /**
     * Vérifie les contrainte d'enregistrement
     *
     * @param array $data
     *
     * @return array
     */
    public function sanitize(array $data = []): array
    {
        //fonction pour securiser les donnees à enregistree
        $data = $this->secure_form($data);

        $number     = $data['number'];
        $city       = strtoupper($data['city']);
        $country    = strtoupper($data['country']);
        $street     = strtoupper($data['street']);
        $postalCode = strtoupper($data['postalCode']);
        $idContact  = intval($data['idContact']);

        if ($number && $city && $country && $postalCode && $street && $idContact) {
            return [
                'response'   => true,
                'number'     => $data['number'],
                'city'       => strtoupper($data['city']),
                'country'    => strtoupper($data['country']),
                'postalCode' => $postalCode,
                'street'     => strtoupper($data['street']),
                'idContact'  => $data['idContact']
            ];
        } else {
            return ['response' => false];
        }
    }
}