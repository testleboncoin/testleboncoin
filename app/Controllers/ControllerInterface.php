<?php

namespace App\Controllers;

interface ControllerInterface
{
    /**
     * Methode pour page d'accueil
     */
    public function index();

    /**
     * Methode pour page de creation
     */
    public function add();

    /**
     * Methode pour page de modification
     * @param int $iId
     */
    public function edit(int $iId);

    /**
     * Methode pour page de suppression
     * @param int $iId
     */
    public function delete(int $iId);

    /**
     * @param array $aData
     * @return array
     */
    public function sanitize(array $aData = []): array;
}