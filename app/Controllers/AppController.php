<?php

namespace App\Controllers;

use App;
use App\Controllers\MainController;
use App\Components\Auth\Auth;

class AppController extends MainController
{
    /**
     * AppController constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $app = App::getInstance();
        $auth = new auth($app->getDatabase());
        if (!$auth->logged()) {
            $this->forbidden();
        }
    }

    /**
     * Méthode de redirection d'un utilisateur vers la page de login
     * @throws \Exception
     */
    protected function forbidden()
    {
        header('HTTP/1.0 403 Forbidden');

        $this->redirect("User/login");
    }

    /**
     * Méthode de chargement de model
     * @param $model
     */
    public function loadModel($model)
    {
        $this->$model = App::getInstance()->getModel($model);
    }
}