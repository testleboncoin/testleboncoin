<?php

namespace App\Controllers;

use App;
use App\Components\Api\Api;
use App\Components\Auth\Auth;
use App\Models\AddresseModel;
use App\Models\ContactModel;
use Dispatcher;
use Exception;
use Request;
use Router;
use \Twig_Loader_Filesystem;
use \Twig_Environment;

abstract class MainController
{
    const PATH_PROJECT = '/testleboncoin/';

    /** @var Twig_Environment */
    protected $twig;

    /** @var Auth */
    protected $auth;

    /** @var Twig_Loader_Filesystem */
    protected $loader;

    /** @var Api*/
    protected $api;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->loader = new \Twig_Loader_Filesystem(ROOT . '/app/Views');
        $this->twig   = new \Twig_Environment($this->loader);
        $this->auth   = new Auth(App::getInstance()->getDatabase());
        $this->api    = new Api();
        $this->twig->addGlobal('session', $_SESSION);
    }

    /**
     * Méthode de chargement de model
     *
     * @param $model
     * @return mixed
     */
    public function loadModel($model)
    {
        return App::getInstance()->getModel($model);
    }

    /**
     * @param $methode
     * @param array $datas
     * @return mixed
     */
    public function apiClient($methode, $datas = [])
    {
        $api = $_SERVER['HTTP_HOST'] . self::PATH_PROJECT . $methode;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $api);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($curl);
        curl_close($curl);

        return json_decode($return);
    }

    /**
     * @param $sActionPath
     * @param int $iParameter
     * @return mixed
     * @throws Exception
     */
    public function redirect($sActionPath, int $iParameter = null)
    {
        $sActionControllerName = explode('/', $sActionPath);
        if (!empty($sActionPath) && count($sActionControllerName) == 2) {
            $controller = '\App\Controllers\\' . ucfirst($sActionControllerName[0]) . 'Controller';
            $action     = $sActionControllerName[1];

            $request = new Request();
            $request->setController(ucfirst($sActionControllerName[0]) . 'Controller');
            $request->setAction($action);
            $request->setParams($iParameter);
            //à mettre l'url dynamique
            if (!empty($iParameter)) {
                $request->setUrl(self::PATH_PROJECT . $sActionPath . '/' . $iParameter);
            } else {
                $request->setUrl(self::PATH_PROJECT . $sActionPath);
            }

            $controller = new $controller();
            header('Location: ' . $request->getUrl());

            return call_user_func_array([$controller, $request->getAction()], $request->getParams());

        } else {
            throw new Exception('Afficher une exception Url not found et faire la redirection');
        }
    }

    /**
     * @param $data
     * @return string
     */
    private function secure_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

        return $data;
    }

    /**
     * @param $form
     * @return mixed
     */
    protected function secure_form($form)
    {
        foreach ($form as $key => $value)
        {
            $form[$key] = $this->secure_input($value);
        }

        return $form;
    }
}