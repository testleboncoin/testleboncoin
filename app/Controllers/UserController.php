<?php

namespace App\Controllers;

use App\Controllers\MainController;


class UserController extends MainController
{
    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function login()
    {
        $errors = false;
        if (!empty($_POST)) {
            if ($this->auth->login($_POST['login'], $_POST['password'])) {
                return $this->redirect("Contact/index");
            } else {
                $errors = true;
            }
        }
        echo $this->twig->render('user/login.html.twig', ['errors' => $errors]);
    }
}