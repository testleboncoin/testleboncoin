-- Sql - table users

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Sql - table contacts

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(225) NOT NULL,
  `prenom` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Sql - table addresses

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(4) NOT NULL,
  `street` varchar(225) NOT NULL,
  `postalCode` int(6) NOT NULL,
  `city` varchar(225) NOT NULL,
  `country` varchar(225) NOT NULL,
  `idContact` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idContact` (`idContact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Sql - contraintes

ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`idContact`) REFERENCES `contacts` (`id`) ON DELETE CASCADE;

ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

-- Sql - initialisation user de test

INSERT INTO `users` (`id`, `login`, `email`, `password`) VALUES
(1, 'admin', 'lebonoin@test.fr', '21232f297a57a5a743894a0e4a801fc3');
COMMIT;

-- Sql --------------------------------------------------------------------

UPDATE `users`
SET `password` = '$2y$10$fPK7ElfQpwlVhF1DWfScIe/qyOJNoGPXl.noBV1v47r9TZro/T5pO'
WHERE `users`.id = 1;

-- Sql - initialisation contact de test

INSERT INTO `contacts` (`id`, `nom`, `prenom`, `email`, `userId`) VALUES
(1, 'labiadh', 'chiheb', 'labiadh.chiheb.1@gmail.com', '1');
INSERT INTO `contacts` (`id`, `nom`, `prenom`, `email`, `userId`) VALUES
(2, 'labiadh', 'patrick', 'labiadh.chiheb.2@gmail.com', '1');
INSERT INTO `contacts` (`id`, `nom`, `prenom`, `email`, `userId`) VALUES
(2, 'labiadh', 'test', 'labiadh.chiheb.3@gmail.com', '1');
COMMIT;

-- Sql - Améliorer la BDD

--- Diminuer le nombre de caractéres
---Economiser de l’espace mémoire
ALTER TABLE users
  MODIFY login varchar(40);
ALTER TABLE users
  MODIFY email varchar(40);

ALTER TABLE contacts
  MODIFY nom varchar(30);
ALTER TABLE contacts
  MODIFY prenom varchar(30);
ALTER TABLE contacts
  MODIFY email varchar(40);

ALTER TABLE addresses
  MODIFY city varchar(30);
ALTER TABLE addresses
  MODIFY country varchar(30);

--- Ajouter les index
--- L’utilisation d’index est indispensable pour conserver de bonnes performances lors de la lecture des données.
CREATE UNIQUE INDEX ind_login
  ON users (login);
CREATE INDEX ind_email
  ON users (email);
CREATE INDEX ind_login_password
  ON users (login, password);
CREATE INDEX ind_email_password
  ON users (email, password);

CREATE FULLTEXT INDEX ind_nom
  ON contacts (nom);
CREATE FULLTEXT INDEX ind_prenom
  ON contacts (prenom);
CREATE FULLTEXT INDEX ind_nom_prenom
  ON contacts (nom, prenom);
